## Contacts

## About Me

Working in Holistic Digital Solutions, Novi Sad. Mainly focusing on the BE in my career, but this only FE project was very interesting to me

## Run

To run json-server you need to go to root folder (contacts/) run `npm run json:server`. It works on 4000 port
<br />
To run project, also go to root folder (contacts/) and then type `npm start`. It works on 3000 port

## The Stack

This Contacts was built using:

- ReactJS
- Material UI
- TypeScript (basic)

It took me about 5 days of coding after work to build this project start to finish.

## Key Features

- CRUD operations
- Search function
- Favourites
