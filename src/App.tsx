import React from "react";
import "./App.css";
import { Route, Routes, BrowserRouter as Router } from "react-router-dom";
import SidemenuDrawer from "./components/layout/SidemenuDrawer";
import EditContact from "./components/pages/contacts/EditContact";

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<SidemenuDrawer />} />

        <Route path="/contacts/:id" element={<EditContact />} />
      </Routes>
    </Router>
  );
}

export default App;
