import React, { useEffect } from "react";
import { useContactsService } from "../../../services/contacts/contacts.api";
import ContactsTable from "../shared/ContactsTable";

export default function Favorites({
  version,
  setVersion,
}: {
  version: number;
  setVersion: any;
}) {
  const { contacts, getContacts } = useContactsService();

  useEffect(() => {
    getContacts();
  }, []);

  return (
    <>
      <h1>Favorites</h1>
      <ContactsTable
        contacts={contacts.filter((contact) => contact.isFavorited)}
        version={version}
        setVersion={setVersion}
      />
    </>
  );
}
