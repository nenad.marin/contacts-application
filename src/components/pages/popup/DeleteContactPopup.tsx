import React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import { DialogContentText } from "@mui/material";

function DeleteContactPopup({
  open,
  setOpen,
  deleteContact,
  idToDelete,
  version,
  setVersion,
}: {
  open: boolean;
  setOpen: any;
  deleteContact: any;
  idToDelete: number | undefined;
  version: number;
  setVersion: any;
}) {
  const handleClose = () => {
    setOpen(false);
    setVersion(version + 1);
  };

  const handleSubmit = async (e: { preventDefault: () => void }) => {
    e.preventDefault();
    await deleteContact(idToDelete);
    setVersion(version + 1);
  };

  return (
    <div>
      <Dialog open={open} onClose={handleClose} fullWidth maxWidth="sm">
        <DialogTitle>Delete contact</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Are you sure you want to delete this contact?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button variant="contained" color="error" onClick={handleSubmit}>
            Save
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default DeleteContactPopup;
