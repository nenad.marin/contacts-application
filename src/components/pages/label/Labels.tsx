import { useEffect } from "react";
import { useLabelById } from "../../../services/labels/labels.api";
import ContactsTable from "../shared/ContactsTable";

export default function Labels({
  id,
  version,
  setVersion,
}: {
  id: number;
  version: number;
  setVersion: any;
}) {
  const { label, getLabelById } = useLabelById();

  useEffect(() => {
    getLabelById(id);
  }, []);

  return (
    <>
      <h1>{label?.name} contacts</h1>
      <ContactsTable
        contacts={label?.contacts}
        version={version}
        setVersion={setVersion}
      />
    </>
  );
}
