import { Contact } from "../contacts/Contact";

export interface Label {
  id: number;
  name: string;
  contacts: Contact[];
}
