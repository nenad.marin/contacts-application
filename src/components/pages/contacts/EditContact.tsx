import { useEffect } from "react";
import { useParams } from "react-router-dom";
import { useContactById } from "../../../services/contacts/contacts.api";
import EditContactForm from "./EditContactForm";

export default function EditContact() {
  const { id } = useParams();
  const { contact, getContactById } = useContactById();
  useEffect(() => {
    getContactById(id);
  }, []);

  return <EditContactForm contact={contact} />;
}
