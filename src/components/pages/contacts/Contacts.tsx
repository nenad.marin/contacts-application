import { useEffect } from "react";
import { useContactsService } from "../../../services/contacts/contacts.api";
import ContactsTable from "../shared/ContactsTable";

export default function Contacts({
  version,
  setVersion,
}: {
  version: number;
  setVersion: any;
}) {
  const { contacts, getContacts } = useContactsService();

  useEffect(() => {
    getContacts();
  }, []);

  return (
    <>
      <h1>Contacts</h1>
      <ContactsTable
        contacts={contacts}
        version={version}
        setVersion={setVersion}
      />
    </>
  );
}
