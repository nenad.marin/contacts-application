export interface Contact {
  id: number | undefined;
  fullName: string | undefined;
  isFavorited: boolean | undefined;
  email: string | undefined;
  phoneNumber: string | undefined;
  profilePicture: string | undefined;
  labelId: string | undefined;
}
