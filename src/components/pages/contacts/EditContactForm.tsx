import { Button, Grid, TextField } from "@mui/material";
import React, { useEffect, useState } from "react";
import ImageUploading, { ImageListType } from "react-images-uploading";
import FormControl from "@mui/material/FormControl";
import { Contact } from "./Contact";
import { useEditContact } from "../../../services/contacts/contacts.api";
import { useLabelsService } from "../../../services/labels/labels.api";

export default function EditContactForm({
  contact,
}: {
  contact: Contact | undefined;
}) {
  const { editContact } = useEditContact();
  const [images, setImages] = React.useState([]);
  const { result, getLabels } = useLabelsService();

  const [state, setState] = useState<any>({
    id: contact?.id,
    fullName: contact?.fullName,
    isFavorited: contact?.isFavorited,
    email: contact?.email,
    phoneNumber: contact?.phoneNumber,
    profilePicture: contact?.profilePicture,
    labelId: contact?.labelId,
  });

  useEffect(() => {
    getLabels();
    setState(contact);
  }, [contact]);

  const handleChange = (e: { target: { name: any; value: any } }) => {
    const { name, value } = e.target;
    setState((prevState: any) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleChangeSelect = (event: { target: { value: any } }) => {
    const {
      target: { value },
    } = event;
    if (value) {
      state.labelId = result
        ?.filter((label) => label.name === value)[0]
        .id.toString();
    }
  };

  const handleSubmit = (e: { preventDefault: () => void }) => {
    e.preventDefault();
    editContact(state);
  };

  const onChange = (imageList: ImageListType) => {
    setImages(imageList as never[]);
    setState({ ...state, profilePicture: imageList[0]?.dataURL });
  };

  return (
    <form noValidate onSubmit={handleSubmit}>
      <ImageUploading value={images} onChange={onChange}>
        {({ imageList, onImageUpload, dragProps }) => {
          return (
            <div className="upload__image-wrapper">
              <Grid container spacing={1}>
                <Grid item sm={3} md={1} lg={1}>
                  <div className="image-item">
                    {imageList[0] ? (
                      <img
                        src={imageList[0]?.dataURL}
                        alt="avatar"
                        style={{
                          width: "45px",
                          height: "45px",
                          borderRadius: "50%",
                          display: "inline-block",
                        }}
                      />
                    ) : (
                      <img
                        style={{
                          width: "45px",
                          height: "45px",
                          borderRadius: "50%",
                          display: "inline-block",
                        }}
                        src={state?.profilePicture}
                        alt="avatar"
                      />
                    )}
                  </div>
                </Grid>
                <Grid item sm={3} md={1} lg={1}>
                  <Button
                    style={{ marginTop: "7px" }}
                    variant="outlined"
                    component="label"
                    onClick={onImageUpload}
                    {...dragProps}
                  >
                    Upload
                  </Button>
                </Grid>
                <Grid item sm={3} md={2} lg={2} />
                <Grid item sm={9} md={9} lg={2}>
                  <FormControl sx={{ m: 1, width: 300 }}>
                    <select
                      name="labelId"
                      value={state?.labelId}
                      onChange={handleChangeSelect}
                    >
                      {result?.map((label) => (
                        <option key={label?.id} value={label?.id}>
                          {label?.name}
                        </option>
                      ))}
                    </select>
                  </FormControl>
                </Grid>
              </Grid>
            </div>
          );
        }}
      </ImageUploading>
      <Grid container spacing={3}>
        <Grid item sm={6}>
          <TextField
            margin="normal"
            required
            fullWidth
            value={state?.fullName}
            defaultValue={state?.fullName}
            onChange={handleChange}
            name="fullName"
            type="text"
            id="fullName"
          />
          <TextField
            margin="normal"
            value={state?.email}
            onChange={handleChange}
            required
            fullWidth
            name="email"
            type="email"
            id="email"
          />

          <TextField
            value={state?.phoneNumber}
            onChange={handleChange}
            margin="normal"
            required
            fullWidth
            name="phoneNumber"
            type="text"
            id="phoneNumber"
          />
          <Grid container spacing={1}>
            <Grid item sm={6}>
              <Button
                variant="outlined"
                onClick={() => (window.location.href = "/")}
              >
                Cancel
              </Button>
            </Grid>
            <Grid item sm={6}>
              <Button type="submit" variant="contained" color="primary">
                Update
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </form>
  );
}
