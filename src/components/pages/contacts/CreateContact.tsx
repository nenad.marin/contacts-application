import { Button, Grid, TextField } from "@mui/material";
import React, { useEffect } from "react";
import ImageUploading, { ImageListType } from "react-images-uploading";
import FormControl from "@mui/material/FormControl";
import MenuItem from "@mui/material/MenuItem";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import InputLabel from "@mui/material/InputLabel";
import OutlinedInput from "@mui/material/OutlinedInput";
import { useLabelsService } from "../../../services/labels/labels.api";
import UploadPhoto from "../../../images/Avatar.png";
import { Contact } from "./Contact";
import { useAddNewContact } from "../../../services/contacts/contacts.api";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

export default function CreateContact() {
  const { addNewContact } = useAddNewContact();
  const { result, getLabels } = useLabelsService();

  const [images, setImages] = React.useState([]);
  const [labelName, setLabelName] = React.useState<string>();
  const [state, setState] = React.useState<Contact>({
    id: 0,
    fullName: "",
    isFavorited: false,
    email: "",
    phoneNumber: "",
    profilePicture: UploadPhoto,
    labelId: "",
  });

  useEffect(() => {
    getLabels();
  }, []);

  const handleChange = (e: { target: { name: any; value: any } }) => {
    const { name, value } = e.target;
    setState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleChangeSelect = (event: SelectChangeEvent<typeof labelName>) => {
    const {
      target: { value },
    } = event;
    setLabelName(value);
    state.labelId = result
      ?.filter((label) => label.name === value)[0]
      .id.toString();
  };

  const handleSubmit = (e: { preventDefault: () => void }) => {
    e.preventDefault();
    addNewContact(state);
  };

  const onChange = (imageList: ImageListType) => {
    setImages(imageList as never[]);
    setState({ ...state, profilePicture: imageList[0]?.dataURL });
  };

  return (
    <form noValidate onSubmit={handleSubmit}>
      <ImageUploading value={images} onChange={onChange}>
        {({ imageList, onImageUpload, dragProps }) => (
          <div className="upload__image-wrapper">
            <Grid container sm={3} md={5} lg={6} spacing={3}>
              <Grid item sm={3} md={5} lg={1}>
                <div className="image-item">
                  {imageList[0] ? (
                    <img
                      src={imageList[0]?.dataURL}
                      alt="avatar"
                      style={{
                        width: "45px",
                        height: "45px",
                        borderRadius: "50%",
                        display: "inline-block",
                      }}
                    />
                  ) : (
                    <img src={UploadPhoto} alt="avatar" />
                  )}
                </div>
              </Grid>
              <Grid item sm={3} md={5} lg={2}>
                <Button
                  style={{ marginTop: "7px" }}
                  variant="outlined"
                  component="label"
                  onClick={onImageUpload}
                  {...dragProps}
                >
                  Upload
                </Button>
              </Grid>
              <Grid item sm={3} md={5} lg={3} />
              <Grid item sm={3} md={5} lg={6}>
                <FormControl sx={{ m: 1, width: 300 }}>
                  <InputLabel id="demo-multiple-name-label">Label</InputLabel>
                  <Select
                    labelId="demo-multiple-name-label"
                    id="demo-multiple-name"
                    value={labelName}
                    onChange={handleChangeSelect}
                    input={<OutlinedInput label="Name" />}
                    MenuProps={MenuProps}
                  >
                    {result?.map((label) => (
                      <MenuItem key={label.name} value={label.name}>
                        {label.name}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>
            </Grid>
          </div>
        )}
      </ImageUploading>
      <Grid container spacing={3}>
        <Grid item sm={6} spacing={3}>
          <TextField
            margin="normal"
            required
            fullWidth
            value={state.fullName}
            onChange={handleChange}
            name="fullName"
            label="Name"
            type="text"
            id="fullName"
          />
          <TextField
            margin="normal"
            value={state.email}
            onChange={handleChange}
            required
            fullWidth
            name="email"
            label="Email address"
            type="text"
            id="email"
          />
          <TextField
            value={state.phoneNumber}
            onChange={handleChange}
            margin="normal"
            required
            fullWidth
            name="phoneNumber"
            label="Phone number"
            type="text"
            id="phoneNumber"
          />
          <Grid container sm={4} spacing={1}>
            <Grid item sm={6}>
              <Button
                variant="outlined"
                onClick={() => (window.location.href = "/")}
              >
                Cancel
              </Button>
            </Grid>
            <Grid item sm={6}>
              <Button type="submit" variant="contained" color="primary">
                Create
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </form>
  );
}
