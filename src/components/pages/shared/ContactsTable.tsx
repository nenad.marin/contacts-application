import React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { Link } from "react-router-dom";
import { Button } from "@mui/material";
import { Contact } from "../contacts/Contact";
import FavoriteIcon from "../../../images/favoriteIcon.svg";
import NotFavoriteIcon from "../../../images/notFavoriteIcon.svg";
import DeleteIcon from "../../../images/deleteIcon.svg";
import EditIcon from "../../../images/editIcon.svg";
import {
  useContactFavoriteToggle,
  useDeleteContact,
} from "../../../services/contacts/contacts.api";
import DeleteContactPopup from "../popup/DeleteContactPopup";
import SearchComponent from "./SearchComponent";

export default function ContactsTable({
  contacts,
  version,
  setVersion,
}: {
  contacts: Contact[] | undefined;
  version: number;
  setVersion: any;
}) {
  const { favoriteContact } = useContactFavoriteToggle();
  const { deleteContact } = useDeleteContact();
  const handleChangeFavorite = (contactToChange: Contact) => {
    setVersion(version + 1);
    favoriteContact(contactToChange);
  };

  const [open, setOpen] = React.useState(false);
  const [searchInput, setSearchInput] = React.useState("");
  const [idToDelete, setIdToDelete] = React.useState<number | undefined>(0);

  const handleDeletePopup = (id: number | undefined) => {
    setOpen(true);
    setIdToDelete(id);
  };

  const handleChangeSearch = (e: {
    target: { value: React.SetStateAction<string> };
  }) => {
    setSearchInput(e.target.value);
  };

  return (
    <>
      <SearchComponent
        searchInput={searchInput}
        handleChangeSearch={handleChangeSearch}
      />
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>NAME</TableCell>
              <TableCell align="left">EMAIL</TableCell>
              <TableCell align="center">PHONE NUMBER</TableCell>
              <TableCell align="center" style={{ width: "1em" }} />
              <TableCell align="center" style={{ width: "1em" }} />
              <TableCell align="center" style={{ width: "1em" }} />
            </TableRow>
          </TableHead>
          <TableBody>
            {contacts
              ?.filter(
                (contact) =>
                  searchInput === "" ||
                  contact.fullName?.toLowerCase().includes(searchInput) ||
                  contact.email?.toLowerCase().includes(searchInput) ||
                  contact.phoneNumber?.toLowerCase().includes(searchInput),
              )
              .map((contact: Contact) => (
                <TableRow
                  key={contact.id}
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    {contact.fullName}
                  </TableCell>
                  <TableCell align="left">{contact.email}</TableCell>
                  <TableCell align="center">{contact.phoneNumber}</TableCell>
                  <TableCell
                    onClick={() => handleChangeFavorite(contact)}
                    align="center"
                  >
                    {contact.isFavorited ? (
                      <img
                        className="iconClass"
                        src={FavoriteIcon}
                        alt="favorited contact"
                      />
                    ) : (
                      <img
                        className="iconClass"
                        src={NotFavoriteIcon}
                        alt="not favorited contact"
                      />
                    )}
                  </TableCell>
                  <TableCell align="center">
                    <Button onClick={() => handleDeletePopup(contact?.id)}>
                      <img src={DeleteIcon} alt="delete contact" />
                    </Button>
                  </TableCell>
                  <TableCell align="center">
                    <Link to={`/contacts/${contact.id}`}>
                      <img src={EditIcon} alt="edit contact" />
                    </Link>
                  </TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </TableContainer>
      <DeleteContactPopup
        open={open}
        setOpen={setOpen}
        deleteContact={deleteContact}
        idToDelete={idToDelete}
        version={version}
        setVersion={setVersion}
      />
    </>
  );
}
