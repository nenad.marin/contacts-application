import { AppBar } from "@mui/material";
import React from "react";
import PrimarySearchAppBar from "../../search/Search";

const drawerWidth = 255;

export default function SearchComponent({
  searchInput,
  handleChangeSearch,
}: {
  searchInput: string;
  handleChangeSearch: any;
}) {
  return (
    <AppBar
      position="fixed"
      sx={{
        width: `calc(100% - ${drawerWidth}px)`,
        ml: `${drawerWidth}px`,
      }}
    >
      <PrimarySearchAppBar
        searchInput={searchInput}
        handleChangeSearch={handleChangeSearch}
      />
    </AppBar>
  );
}
