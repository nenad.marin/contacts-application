import React from "react";
import { Button } from "@mui/material";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import Divider from "@mui/material/Divider";
import Drawer from "@mui/material/Drawer";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import AddIcon from "@mui/icons-material/Add";
import PersonIcon from "@mui/icons-material/Person";
import FavoritesIcon from "@mui/icons-material/StarOutline";
import { indigo } from "@mui/material/colors";
import LabelIcon from "@mui/icons-material/BookmarkBorder";
import AppLogo from "../../images/Mark.svg";
import "./css/SidemenuDrawer.css";
import { Label } from "../pages/label/Label";
import { Contact } from "../pages/contacts/Contact";

const drawerWidth = 255;

const buttonTheme = createTheme({
  palette: {
    primary: {
      main: indigo[600],
    },
  },
});

const buttonTextTheme = createTheme({
  palette: {
    primary: {
      main: "#ffffff",
    },
  },
});

export default function SidemenuItem({
  setValue,
  result,
  contacts,
  setOpen,
}: {
  setValue: any;
  result: Label[];
  contacts: Contact[];
  setOpen: any;
}) {
  const handleChange = (event: any, newValue: React.SetStateAction<number>) => {
    setValue(newValue);
  };

  const handleLabelCreatePopup = (openValue: boolean) => {
    setOpen(openValue);
  };

  const drawer = (
    <div>
      <div className="logoModule">
        <img src={AppLogo} alt="App logo" />
        <span className="logoText">Contacts</span>
      </div>
      <div className="createButton">
        <ThemeProvider theme={buttonTheme}>
          <Button
            variant="contained"
            onClick={(e) => handleChange(e, -1)}
            startIcon={<AddIcon />}
          >
            <ThemeProvider theme={buttonTextTheme}>
              <span>Create contact</span>
            </ThemeProvider>
          </Button>
        </ThemeProvider>
      </div>
      <List>
        <ListItem key="Contacts" disablePadding>
          <ListItemButton onClick={(e) => handleChange(e, 0)}>
            <ListItemIcon>
              <PersonIcon />
            </ListItemIcon>
            <ListItemText primary="Contacts" />
            <div className="badgeCount">{contacts.length}</div>
          </ListItemButton>
        </ListItem>
        <ListItem key="Favorites" disablePadding>
          <ListItemButton onClick={(e) => handleChange(e, 1)}>
            <ListItemIcon>
              <FavoritesIcon />
            </ListItemIcon>
            <ListItemText primary="Favorites" />
            <div className="badgeCount">
              {
                contacts.filter((contact: Contact) => contact.isFavorited)
                  .length
              }
            </div>
          </ListItemButton>
        </ListItem>
        <Divider />
        {result.map((label: Label) => (
          <ListItem key={label.name} disablePadding>
            <ListItemButton
              onClick={(e) => handleChange(e, result.indexOf(label) + 2)}
            >
              <ListItemIcon>
                <LabelIcon />
              </ListItemIcon>
              <ListItemText primary={label.name} />
              <div className="badgeCount">{label.contacts.length}</div>
            </ListItemButton>
          </ListItem>
        ))}
      </List>
      <Button
        onClick={() => handleLabelCreatePopup(true)}
        startIcon={<AddIcon />}
      >
        Create label
      </Button>
    </div>
  );

  return (
    <Drawer
      sx={{
        width: drawerWidth,
        flexShrink: 0,
        "& .MuiDrawer-paper": {
          width: drawerWidth,
          boxSizing: "border-box",
        },
      }}
      variant="permanent"
      anchor="left"
    >
      {drawer}
    </Drawer>
  );
}
