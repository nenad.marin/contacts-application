import React, { useEffect } from "react";
import { Box, CssBaseline, Toolbar } from "@mui/material";
import Favorites from "../pages/favorites/Favorites";
import Contacts from "../pages/contacts/Contacts";
import "./css/SidemenuDrawer.css";
import { Label } from "../pages/label/Label";
import { useLabelsService } from "../../services/labels/labels.api";
import { useContactsService } from "../../services/contacts/contacts.api";
import TabPanel from "./TabPanel";
import Labels from "../pages/label/Labels";
import CreateContact from "../pages/contacts/CreateContact";
import CreateLabelPopup from "../pages/popup/CreateLabelPopup";
import SidemenuItem from "./SidemenuItem";

export default function SidemenuDrawer() {
  const { result, getLabels } = useLabelsService();
  const { contacts, getContacts } = useContactsService();
  const [value, setValue] = React.useState(0);
  const [open, setOpen] = React.useState(false);

  const [version, setVersion] = React.useState(0);

  useEffect(() => {
    getLabels();
    getContacts();
  }, []);

  return (
    <Box style={{ display: "flex" }}>
      <CssBaseline />
      <SidemenuItem
        setValue={setValue}
        contacts={contacts}
        result={result}
        setOpen={setOpen}
      />
      <Box
        component="main"
        sx={{ flexGrow: 1, bgcolor: "background.default", p: 3 }}
      >
        <Toolbar />
        <TabPanel value={value} index={-1}>
          {value === -1 && <CreateContact />}
        </TabPanel>
        <TabPanel value={value} index={0}>
          {value === 0 && (
            <Contacts version={version} setVersion={setVersion} />
          )}
        </TabPanel>
        <TabPanel value={value} index={1}>
          {value === 1 && (
            <Favorites version={version} setVersion={setVersion} />
          )}
        </TabPanel>
        {result.map((label: Label) => (
          <TabPanel
            key={label.id}
            value={value}
            index={result.indexOf(label) + 2}
          >
            {value === result.indexOf(label) + 2 && (
              <Labels id={label.id} version={version} setVersion={setVersion} />
            )}
          </TabPanel>
        ))}
      </Box>
      <CreateLabelPopup
        open={open}
        setOpen={setOpen}
        version={version}
        setVersion={setVersion}
      />
    </Box>
  );
}
