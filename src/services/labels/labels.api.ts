import axios from "axios";
import { useState } from "react";
import { Label } from "../../components/pages/label/Label";

export const useLabelsService = () => {
  const [result, setResult] = useState<Label[]>([]);
  const getLabels = async () => {
    const obj = await axios.get(
      `${process.env.REACT_APP_API_URL}/labels?_embed=contacts`,
    );
    setResult(obj.data);
  };

  return { result, getLabels };
};

export const useLabelById = () => {
  const [label, setLabel] = useState<Label>();

  const getLabelById = async (id: number) => {
    const response = await axios.get(
      `${process.env.REACT_APP_API_URL}/labels/${id}?_embed=contacts`,
    );
    setLabel(response.data);
  };

  return { label, getLabelById };
};

export const useAddNewLabel = () => {
  const addNewLabel = async (label: any) => {
    const labels = await axios.get(`${process.env.REACT_APP_API_URL}/labels`);
    label.id = (labels.data.length + 1).toString();
    await axios.post(`${process.env.REACT_APP_API_URL}/labels/`, label);
  };

  return { addNewLabel };
};
