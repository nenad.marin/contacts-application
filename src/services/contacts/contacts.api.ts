import axios from "axios";
import { useState } from "react";
import { Contact } from "../../components/pages/contacts/Contact";

export const useContactsService = () => {
  const [contacts, setResult] = useState<Contact[]>([]);
  const getContacts = async () => {
    const obj = await axios.get(`${process.env.REACT_APP_API_URL}/contacts`);
    setResult(obj.data);
  };

  return { contacts, getContacts };
};

export const useContactById = () => {
  const [contact, setContact] = useState<Contact>();

  const getContactById = async (id: string | undefined) => {
    const response = await axios.get(
      `${process.env.REACT_APP_API_URL}/contacts/${id}`,
    );
    setContact(response.data);
  };

  return { contact, getContactById };
};

export const useContactFavoriteToggle = () => {
  const [contact, setContact] = useState<Contact>();

  const favoriteContact = async (contactToChange: Contact) => {
    contactToChange.isFavorited = !contactToChange.isFavorited;
    const response = await axios.put(
      `${process.env.REACT_APP_API_URL}/contacts/${contactToChange.id}`,
      contactToChange,
    );
    setContact(response.data);
  };

  return { contact, favoriteContact };
};

export const useAddNewContact = () => {
  const addNewContact = async (contact: Contact) => {
    const contacts = await axios.get(
      `${process.env.REACT_APP_API_URL}/contacts`,
    );
    contact.id = contacts.data[contacts.data.length - 1].id + 1;
    await axios.post(`${process.env.REACT_APP_API_URL}/contacts/`, contact);
    window.location.href = "/";
  };

  return { addNewContact };
};

export const useEditContact = () => {
  const editContact = async (contact: Contact) => {
    await axios.put(
      `${process.env.REACT_APP_API_URL}/contacts/${contact.id}`,
      contact,
    );
    window.location.href = "/";
  };

  return { editContact };
};

export const useDeleteContact = () => {
  const deleteContact = async (id: any) => {
    await axios.delete(`${process.env.REACT_APP_API_URL}/contacts/${id}`);
    window.location.href = "/";
  };

  return { deleteContact };
};
